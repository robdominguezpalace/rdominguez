# Generated by Django 2.2.3 on 2022-07-15 10:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_remove_user_created'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='firs_name',
            new_name='first_name',
        ),
        migrations.AddField(
            model_name='user',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
