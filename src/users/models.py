from asyncio.windows_events import NULL
from django.db import models

class User(models.Model):
    user = models.CharField(max_length=120)
    password = models.CharField(max_length=120)
    email = models.CharField(max_length=120)
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    created = models.DateTimeField(auto_now_add=True)
