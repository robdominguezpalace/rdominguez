from dataclasses import field, fields
from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    firstName = serializers.CharField(source='first_name')
    lastName = serializers.CharField(source='last_name')

    class Meta:
        model = User
        fields = ('pk','user','password','email','firstName','lastName')