from urllib import response
from users.models import User
from django.http import HttpResponse
from django.views import View
from users.serializers import UserSerializer
from rest_framework.permissions import IsAuthenticated
import json

class UserView(View):
    permission_classes = [IsAuthenticated]
    
    def post(self, request):
        user_json = json.loads(request.body)
        serializer = UserSerializer(data=user_json)
        email = user_json["email"]
        validate_user = self.validate_exist(email)
        message = "User already exists"

        if(validate_user == False):
            if(serializer.is_valid()):
                message = "User created"
                serializer.save()

        user = User.objects.filter(email = email)
        serializerResponse = UserSerializer(user, many = True)
        data = serializerResponse.data
        response = dict(
            data = data,
            message = message
        )

        return HttpResponse(json.dumps(response),  content_type="application/json")
    
    def get(self, request,id):
        user = User.objects.filter(id = id)
        serializer = UserSerializer(user, many = True)
        response = dict(
                data = serializer.data, 
                message = "User found"
            )

        if(user.exists() == False):
            response["message"] = "User not found"
            return HttpResponse(json.dumps(response),status = 204)

        return HttpResponse(json.dumps(response),  content_type="application/json")

    # VALIDO EL EMAIL YA QUE USUALMENTE SUELE SER UN CAMPO UNICO
    @staticmethod
    def validate_exist(email):
        user = User.objects.filter(email = email)

        if(user.exists()):
            return True
        
        return False


